# frozen_string_literal: true

RSpec.describe Bup::Config do
  it "config merges" do
    m = Bup::Config.merge({
                            a: 1,
                            b: {
                              c: [3]
                            }
                          }, {
                            a: 2,
                            b: {
                              c: [3, 4]
                            }
                          })

    expect(m[:a]).to eq(2)
    expect(m[:b][:c][0]).to eq(3)
    expect(m[:b][:c][1]).to eq(3)
    expect(m[:b][:c][2]).to eq(4)
  end
end
