#!/usr/bin/env ruby
# frozen_string_literal: true

require "bup"
require "optparse"

configfile = File.join(ENV["HOME"], ".buprc")
config = Bup::Config.new
profile = nil

OptParse.new do |opt|
  opt.banner = "#{$PROGRAM_NAME} #{Bup::VERSION}"

  opt.on("-c", "--config=file", String, "Config file.") do |c|
    configfile = c
  end

  opt.on("-p", "--profile=profile", String, "Profile name.") do |p|
    profile = p
  end

  opt.on("-i", "--incremental", String, "Set the type of backup to incremental.") do
    config.runtime["type"] = "incremental"
  end

  opt.on("-f", "--full", String, "Set the type of backup to full.") do
    config.runtime["type"] = "full"
  end

  opt.on("-l", "--list", "List profiles and exit.") do
    config.runtime["action"] = "list"
  end
end.parse!

config.load(configfile) if File.exist?(configfile)

if profile
  config.runtime["profile"] = profile
elsif config.config["default_profile"]
  config.runtime["profile"] = config.config["default_profile"]
end

case config.runtime["action"]
when "list"
  config.config["profiles"].each do |key, prof|
    puts "#{key} - #{prof["description"] || ""}"
  end
else
  tar = Bup::Tar.new(config)
  tar.call
  config.save(configfile)
end
