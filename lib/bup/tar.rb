# frozen_string_literal: true

require "tempfile"
require "open3"
require "date"

module Bup
  class Tar
    def initialize(config)
      @config = config
    end

    def expand_string(str)
      while str =~ /\${?([a-zA-Z0-9_]+)}?/
        all = $~[0]
        nm = $~[1]
        str = str.sub(all, ENV[nm] || "")
      end

      str
    end

    def build_exclude_file(patterns)
      tf = Tempfile.new("bup")
      patterns.each do |p|
        p = expand_string(p)
        tf.write(p)
        tf.write("\n")
      end
      tf.close
      tf
    end

    # Prepare the destination directory.
    def prepare_destination(profile_name, profile)
      destination = expand_string(profile["destination"] || ".")

      type = @config.runtime["type"] || "full"

      FileUtils.mkdir_p(destination) unless File.exist?(destination)

      filename_base = "#{profile_name}-#{type}"
      filename = File.join(destination,
                           "#{filename_base}-#{DateTime.now.new_offset(0).strftime("%Y%m%d%H%M%S")}")

      history = @config.profile(profile_name)["history"] || 0

      if type == "full" 
        if history.positive?
          prune_history(profile_name, destination, filename_base, history)
        elsif history == 0
          clear_history(profile_name, destination)
        end
      end

      ENV["BUP_DESTINATION"] = destination
      ENV["BUP_FILENAME"] = filename
      filename
    end

    def clear_history(profile_name, destination)
      Dir["#{destination}/#{profile_name}*"].each do |backup|
        File.delete(backup)
      end
    end

    def prune_history(profile_name, destination, filename_base, history)
      oldest_kept_file = nil

      # Keep some full backups and remove the others.
      # We capture the oldest full backup and get rid of preceeding incrementals.
      Dir["#{destination}/#{filename_base}*"].sort.reverse.each_with_index do |fullbackup, idx|
        if idx < history
          oldest_kept_file = fullbackup
        else
          File.delete(fullbackup)
        end
      end

      # Remove all incremental files that are older than the oldest kept full backup.
      if oldest_kept_file
        remove_before = File.stat(oldest_kept_file).ctime
        Dir["#{destination}/#{profile_name}*"].each do |backupfile|
          File.delete(backupfile) if File.stat(backupfile).ctime < remove_before
        end
      end
    end

    # Run tar.
    def call(profile_name = nil)
      profile_name ||= @config.runtime["profile"]
      profile = @config.config["profiles"][profile_name]

      raise "Missing profile #{profile_name}." unless profile

      args = profile["tarcmd"].dup || ["tar", "cJvf", "${BUP_FILENAME}.tar.xz"]

      prepare_destination(profile_name, profile)

      if @config.runtime["type"] == "incremental"
        lastrun = @config.lastrun(profile_name)
        args += ["--newer", lastrun.strftime("%Y-%m-%d %H:%M:%S %z")] if lastrun
      end

      run_pre_cmds profile

      @config.update_lastrun(profile_name)

      tf = build_exclude_file(profile["exclude"] || [])

      # Build and run tar command.
      begin
        args += ["--exclude-from", tf.path]
        args += (profile["include"] || ["."])
        args = args.map do |str|
          expand_string(str)
        end

        # Run tar.
        run_cmd(*args)
      ensure
        tf.unlink
      end

      run_post_cmds profile
    end

    def run_pre_cmds(profile)
      # Before we update the last_run, execute any pre_cmds.
      (profile["pre_cmds"] || []).each do |args|
        run_cmd(*args.map { |c| expand_string(c) })
      end
    end

    def run_post_cmds(profile)
      (profile["post_cmds"] || []).each do |args|
        run_cmd(*args.map { |c| expand_string(c) })
      end
    end

    # Exec and run a command in a standard way.
    def run_cmd(*args)
      Open3.popen3(*args) do |stdin, stdout, stderr, wait_thr|
        stdin.close
        x = nil
        while wait_thr.status
          r, _w, _e = IO.select([stdout, stderr])
          r.each do |stream|
            print x if (x = stream.read_nonblock(1024, exception: false))
          end
        end

        wait_thr.join
        print x if (x = stdout.read_nonblock(1024, exception: false))

        print x if (x = stderr.read_nonblock(1024, exception: false))

        $stdout.flush
      end
    end
  end
end
