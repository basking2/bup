# frozen_string_literal: true

require_relative "bup/version"
require_relative "bup/config"
require_relative "bup/tar"

module Bup
  class Error < StandardError; end
  # Your code goes here...
end
