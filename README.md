# Bup

Bup is a simple backup driver script that uses `tar` to backup files.
The idea is to simplify creating backups, full and incremental, and leave
restoration as a project for the user to untar the file where their contents
is needed.

Features like backup profiles, file dating, history rotation,
incremental backups, and post-processing scripting is provided.

## Usage

Create a file ~/.buprc with the default contents...

```yaml
---
default_profile: default
profiles:
  default:
    description: Simple backup of critical files.
    include:
    - "$HOME"
    - /$BUP_DESTINATION/db.dump
    exclude:
    - "$HOME"
    - "$HOME/backups"
    lastrun: '2021-11-14T03:06:45 +0000'
    destination: "$HOME/backups"
    history: 2
    tarcmd:
    - tar
    - cJvf
    - "${BUP_FILENAME}.tar.xz"
    pre_cmds:
    - - dumpdatabase
      - /$BUP_DESTINATION/db.dump
    post_cmds:
    - - ls
      - $BUP_FILENAME.tar.xz
    - - rm
      - /$BUP_DESTINATION/db.dump
```

This defines a single profile, "default". You can list
defined profiles by running `bup -l`. You run the default backup profile
simply by running `bup`. Other profiles must be selected on the command
line using `bup -p <profile name>`.

The default profile, as defined above, excludes both the `$HOME` and
`$HOME/backups` directories. Remove `$HOME` from the excludes list to actually
backup your entire home directory.

## Incremental Backups

By calling `bup -t incremental` an incremental backup will be run.
This runs `tar` with the `--newer` option included with the time
specified in the profile's `lastrun` setting. `Tar` will then exclude
files older than that time.

## Backup History

Before a new backup is run, previous backups are removed. If you run many 
uncompleted backups, you will eventually remove all good backups from your
backup history. This behavior is desired because backups should nearly always
succeed and we want to ensure that disk pressure is kep lower than higher.
Higher disk pressure might cause a backup to fail.
