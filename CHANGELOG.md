## [Unreleased]

## [0.8.0] - 2022-04-27

- Drop required Ruby version to 2.7.

## [0.7.0] - 2021-11-29

- BUG FIX: Create destination was not being expanded.

## [0.6.0] - 2021-11-29

- Add pre_cmds list to allow executing command before backup, such as dumping a database.
- Add BUP_DESTINATION that holds the backup destination directory.
  This may be used to put dumped database files in before a backup and clean 
  them up with a post_cmd.
- Refactor the tar.rb file for more simple individual function calls
  and to do a little less redundant processing.
- Replaced --type=[full|incremental] with --incremental and --full options.

## [0.5.0] - 2021-11-13

- Allow for a history size of 0.
- Add default_profile setting.

## [0.4.0] - 2021-11-13

- Bug fix for empty post_cmds list.

## [0.3.0] - 2021-11-13

- Communicate the backup file base name through the environment variable BUP_FILENAME.
- Rubocop fixes or ignore files.
- Add post commands, incase you want to default copy something around.

## [0.2.0] - 2021-11-13

- List profiles.
- Bug fixes against empty backup directories.
- Bug fix to print buffers using read_nonblock from popen3 streams.

## [0.1.0] - 2021-11-11

- Backup profiles.
- Backup history limits.
- Tar command specification.
